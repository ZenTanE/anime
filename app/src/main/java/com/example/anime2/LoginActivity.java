package com.example.anime2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    public static String email;
    public static String password;

    private TextInputLayout textInputLayout_Email;
    private TextInputEditText textInputEditText_Email;

    private TextInputLayout textInputLayout_Password;
    private TextInputEditText textInputEditText_Password;

    private Button button_Login;

    private Button button_SignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Hooks
        textInputLayout_Email = findViewById(R.id.signUp_TextInputLayout_Email);
        textInputEditText_Email = findViewById(R.id.signUp_TextInputEditText_Email);

        textInputLayout_Password = findViewById(R.id.signUp_TextInputLayout_Password);
        textInputEditText_Password = findViewById(R.id.signUp_TextInputEditText_Password);

        button_Login = findViewById(R.id.signUp_Button_Login);

        button_SignUp = findViewById(R.id.signUp_Button_SignUp);

        // Listeners
        button_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // TODO: check for authentication

                email = textInputEditText_Email.getText().toString();
                password = textInputEditText_Password.getText().toString();

                // Change to MainActivity
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });

        button_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Change to SignUpActivity
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);

            }
        });

    }

}