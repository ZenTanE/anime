package com.example.anime2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class SignUpActivity extends AppCompatActivity {

    private TextInputLayout textInputLayout_Name;
    private TextInputEditText textInputEditText_Name;

    private TextInputLayout textInputLayout_Email;
    private TextInputEditText textInputEditText_Email;

    private TextInputLayout textInputLayout_Password;
    private TextInputEditText textInputEditText_Password;

    private TextInputLayout textInputLayout_Phone;
    private TextInputEditText textInputEditText_Phone;

    private Button button_SignUp;

    private Button button_Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // Hooks
        textInputLayout_Name = findViewById(R.id.signUp_TextInputLayout_Name);
        textInputEditText_Name = findViewById(R.id.signUp_TextInputEditText_Name);

        textInputLayout_Email = findViewById(R.id.signUp_TextInputLayout_Email);
        textInputEditText_Email = findViewById(R.id.signUp_TextInputEditText_Email);

        textInputLayout_Password = findViewById(R.id.signUp_TextInputLayout_Password);
        textInputEditText_Password = findViewById(R.id.signUp_TextInputEditText_Password);

        textInputLayout_Phone = findViewById(R.id.signUp_TextInputLayout_Phone);
        textInputEditText_Phone = findViewById(R.id.signUp_TextInputEditText_Phone);

        button_SignUp = findViewById(R.id.signUp_Button_Login);

        button_Login = findViewById(R.id.signUp_Button_SignUp);

        // Listeners
        button_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // TODO: Sign up the user

                // Change to MainActivity
                Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });

        button_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Change to LoginActivity
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });

    }

}