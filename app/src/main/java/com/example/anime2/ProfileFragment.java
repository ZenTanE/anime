package com.example.anime2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // The fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String URL_part1 = "https://joanseculi.com/edt69/loginuser.php?email=";
    private String URL_email = "place@holder.com"; // Default value is pointless, as it SHOULD be updated before being used
    private String URL_part2 = "&password=";
    private String URL_password = "placeholder"; // Default value is pointless, as it SHOULD be updated before being used

    private TextInputLayout textInputLayout_Name;
    private TextInputEditText textInputEditText_Name;

    private TextView textView_Email;

    private TextInputLayout textInputLayout_Password;
    private TextInputEditText textInputEditText_Password;

    private TextInputLayout textInputLayout_Phone;
    private TextInputEditText textInputEditText_Phone;

    private Button button_Update;

    private Button button_Delete;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        textInputLayout_Name = view.findViewById(R.id.profile_TextInputLayout_Name);
        textInputEditText_Name = view.findViewById(R.id.profile_TextInputEditText_Name);

        textView_Email = view.findViewById(R.id.profile_TextView_Email);

        textInputLayout_Password = view.findViewById(R.id.profile_TextInputLayout_Password);
        textInputEditText_Password = view.findViewById(R.id.profile_TextInputEditText_Password);

        textInputLayout_Phone = view.findViewById(R.id.profile_TextInputLayout_Phone);
        textInputEditText_Phone = view.findViewById(R.id.profile_TextInputEditText_Phone);

        button_Update = view.findViewById(R.id.profile_Button_Update);

        button_Delete = view.findViewById(R.id.profile_Button_Delete);

        URL_email = LoginActivity.email;
        URL_password = LoginActivity.password;

        RequestQueue queue = Volley.newRequestQueue(getContext());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL_part1 + URL_email + URL_part2 + URL_password,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            // TODO: Fix the invisibility of the text of the TextInputEditText objects (the text IS there, but invisible)
                            textInputEditText_Name.setText(response.getString("name"));
                            textView_Email.setText(response.getString("email"));
                            textInputEditText_Password.setText(String.valueOf(response.getString("password")));
                            textInputEditText_Phone.setText(String.valueOf(response.getString("phone")));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.d("tag", "onErrorResponse: " + error.getMessage());

                    }
                }
        );

        queue.add(jsonObjectRequest);

        return view;

    }

}