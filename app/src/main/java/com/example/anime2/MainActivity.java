package com.example.anime2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView mainNav;
    private FrameLayout mainFrameLayout;

    private ListFragment listFragment;
    private ProfileFragment profileFragment;
    private FavoritesFragment favoritesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainNav = findViewById(R.id.main_BottomNavigationView);
        mainFrameLayout = findViewById(R.id.main_FrameLayout);

        listFragment = new ListFragment();
        profileFragment = new ProfileFragment();
        favoritesFragment = new FavoritesFragment();

        setFragment(listFragment);

        mainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.nav_list:

                        // Change to ListFragment
                        setFragment(listFragment);

                        return true;
                    case R.id.nav_profile:

                        // Change to ProfileFragment
                        setFragment(profileFragment);

                        return true;
                    case R.id.nav_favorites:

                        // Change to FavoritesFragment
                        setFragment(favoritesFragment);

                        return true;
                    default:
                        return false;
                }

            }

        });

    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_FrameLayout, fragment);
        fragmentTransaction.commit();
    }


}