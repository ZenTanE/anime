package com.example.anime2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    Context context;
    List<Anime> animeList;

    public MyAdapter(Context context, List<Anime> animeList) {

        this.context = context;
        this.animeList = animeList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.anime_row, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Picasso.get().load(animeList.get(position).getImage())
                .fit()
                .centerCrop()
                .into(holder.image);
        holder.name.setText(animeList.get(position).getName());
        holder.description.setText(animeList.get(position).getDescription());
        holder.year.setText(String.valueOf(animeList.get(position).getYear()));
        holder.type.setText(animeList.get(position).getType());

    }

    @Override
    public int getItemCount() {
        return animeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout layout;
        ImageView image;
        TextView name;
        TextView description;
        TextView year;
        TextView type;
        //ImageView favorite;

        public MyViewHolder(@NonNull View itemView) {

            super(itemView);
            layout = itemView.findViewById(R.id.anime_ConstraintLayout);
            image = itemView.findViewById(R.id.anime_ImageView_Image);
            name = itemView.findViewById(R.id.anime_TextView_Name);
            description = itemView.findViewById(R.id.anime_TextView_Description);
            year = itemView.findViewById(R.id.anime_TextView_Year);
            type = itemView.findViewById(R.id.anime_TextView_Type);
            //favorite = itemView.findViewById(R.id.anime_ImageView_Favorite);

        }
    }
}
